package Problems;

public class TwoSum_1 {

    public static void main(String[] arg) {

        int[] sumArray = {2, 7, 11, 15};
        int[] answer;
        answer = twoSum(sumArray, 9 );

        for (int value : answer) {
            System.out.println(value);
        }
    }

    public static int[] twoSum(int[] nums, int target) {

        int start = 0;
        int end = nums.length;

        for(int i = start; i < end; i++){
            for(int j = i+1 ; j < end; j++){
                if(nums[i] + nums[j] == target){
                    return new int[] {i,j};
                }
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}

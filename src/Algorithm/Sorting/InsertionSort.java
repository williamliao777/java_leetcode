package Algorithm.Sorting;

public class InsertionSort {

    public static void main(String[] arg) {

        int[] sortArray = {20,35,-15,55,7};

        for(int firstUnsortedIndex = 1 ; firstUnsortedIndex < sortArray.length ; firstUnsortedIndex++){
            int newElement = sortArray[firstUnsortedIndex];
            int i ;
            for(i = firstUnsortedIndex ; i > 0 &&  sortArray[i-1] > newElement; i --){
                sortArray[i] = sortArray[i-1];
            }

            sortArray[i] = newElement;
        }

        for (int value : sortArray) {
            System.out.println(value);
        }
    }
}

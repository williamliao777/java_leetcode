package Algorithm.Sorting;

public class SelectionSort {

    public static void main(String[] arg) {

        // [7,35,1,25]
        int[] sortArray = {7,35,1,25,-5};

        for(int lastUnsortedIndex = sortArray.length-1; lastUnsortedIndex > 0 ; lastUnsortedIndex --){
            int bigNumIndex = 0;
            for(int j = 1; j <= lastUnsortedIndex ; j++){
                if(sortArray[bigNumIndex] < sortArray[j]){
                    bigNumIndex = j;
                }
            }
            swap(sortArray, bigNumIndex, lastUnsortedIndex);
        }

        for (int value : sortArray) {
            System.out.println(value);
        }

    }

    public static void swap(int[] swapArray, int itemIndexX, int itemIndexY){
        int temp = swapArray[itemIndexX];
        swapArray[itemIndexX] = swapArray[itemIndexY];
        swapArray[itemIndexY] = temp;
    }


}

package Algorithm.Sorting;

public class BubbleSort {

    public static void main(String[] arg) {

        // [7,35,1,25]
        int[] sortArray = {7,35,1,25};

        for(int lastUnsortedIndex = sortArray.length-1; lastUnsortedIndex > 0 ; lastUnsortedIndex --){
            for(int j = 0; j < lastUnsortedIndex ; j++){
                if(sortArray[j] > sortArray[j+1]){
                    swap(sortArray, j, j+1);
                }
            }
        }

        for (int value : sortArray) {
            System.out.println(value);
        }

    }

    public static void swap(int[] swapArray, int itemIndexX, int itemIndexY){
        int temp = swapArray[itemIndexX];
        swapArray[itemIndexX] = swapArray[itemIndexY];
        swapArray[itemIndexY] = temp;
    }

}

package Algorithm.Sorting;

public class CountingSort {

    public static void main(String[] arg) {

        int[] unSortArray = {2, 5, 9, 8, 2, 8, 7, 10, 4, 3};

        countingSort(unSortArray, 1, 10);

        for (int value : unSortArray) {
            System.out.println(value);
        }

    }

    public static void countingSort(int[] unSortedArray, int min, int max){

        int[] countingSort = new int[(max - min) +1];

        for(int i = 0; i < unSortedArray.length; i++){
            countingSort[unSortedArray[i] - min]++;
        }

        int j = 0;
        for(int i = min; i<= max; i++){
            while(countingSort[ i - min] > 0){
                unSortedArray[j++] = i;
                countingSort[i - min]--;
            }
        }

    }

}

package Algorithm.Sorting;

public class InsertionSortRecursive {

    public static void main(String[] arg) {

        int[] sortArray = {20,35,-15,55,7};
        insertionSort(sortArray, sortArray.length);

        for (int value : sortArray) {
            System.out.println(value);
        }
    }

    public static void insertionSort(int[] unSortedArray, int numOfItem){

        if(numOfItem < 2){
            return;
        }

        insertionSort(unSortedArray, numOfItem-1);

        int newElement = unSortedArray[numOfItem-1];

        int i ;
        for(i = numOfItem-1 ; i > 0 &&  unSortedArray[i-1] > newElement; i --){
            unSortedArray[i] = unSortedArray[i-1];
        }

        unSortedArray[i] = newElement;
    }
}

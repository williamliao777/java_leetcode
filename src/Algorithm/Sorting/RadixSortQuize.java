package Algorithm.Sorting;

public class RadixSortQuize {

    public static void main(String[] arg) {

        String[] unSortArray = {"bcdef", "dbaqc", "abcde", "omadd", "bbbbb"};

        radixSort(unSortArray, 26, 5);

        for (String value : unSortArray) {
            System.out.println(value);
        }

    }

    public static void radixSort(String[] unSortedArray, int radix, int width){

        for(int i= 1; i <= width; i++){
            radixSortSingle(unSortedArray, i, radix);
        }
    }

    public static void radixSortSingle(String[] unSortedArray, int position, int radix){

        int unSortedItemCount = unSortedArray.length;
        int[] countingArray = new int[radix];
        //count your value
        for(int i = 0; i < unSortedItemCount; i++){
            countingArray[getEngAlphabetNumber(unSortedArray[i], position)]++;
        }
        //adjust count in order to stabilize
        for(int j = 1; j < countingArray.length; j++){
            countingArray[j] += countingArray[j-1];
        }
        //wright into temp
        String[] temp = new String[unSortedItemCount];
        for(int unSortedIndex = unSortedItemCount-1; unSortedIndex >= 0; unSortedIndex--){
            temp[--countingArray[getEngAlphabetNumber(unSortedArray[unSortedIndex], position)]] = unSortedArray[unSortedIndex];
        }

        //copy temp into unsorted array
        for(int i = 0; i < temp.length; i++){
            unSortedArray[i] = temp[i];
        }

    }

    public static int getEngAlphabetNumber(String value, int position){

        char[] ch = value.toCharArray();
        //for lowerCase
        int temp_integer= 96;
        return (int)ch[ch.length - position] - temp_integer;

    }

}

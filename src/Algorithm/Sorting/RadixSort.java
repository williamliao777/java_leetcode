package Algorithm.Sorting;

public class RadixSort {

    public static void main(String[] arg) {

        int[] unSortArray = {4725, 4586, 1330, 8792, 1594, 5729};

        radixSort(unSortArray, 10, 4);

        for (int value : unSortArray) {
            System.out.println(value);
        }

    }

    public static void radixSort(int[] unSortedArray, int radix, int width){

        for(int i= 0; i < width; i++){
            radixSortSingle(unSortedArray, i, radix);
        }
    }

    public static void radixSortSingle(int[] unSortedArray, int position, int radix){

        int unSortedItemCount = unSortedArray.length;
        int[] countingArray = new int[radix];
        for(int i = 0; i < unSortedItemCount; i++){
            countingArray[getDigit(unSortedArray[i], position, radix)]++;
        }
        //adjust count in order to stabilize
        for(int j = 1; j < countingArray.length; j++){
            countingArray[j] += countingArray[j-1];
        }
        //wright into temp
        int[] temp = new int[unSortedItemCount];
        for(int unSortedIndex = unSortedItemCount-1; unSortedIndex >= 0; unSortedIndex--){
            temp[--countingArray[getDigit(unSortedArray[unSortedIndex], position, radix)]] = unSortedArray[unSortedIndex];
        }

        //copy temp into unsorted array
        for(int i = 0; i < temp.length; i++){
            unSortedArray[i] = temp[i];
        }

    }

    public static int getDigit(int value, int position, int radix){

        //split to understand
        int divisor = (int)Math.pow(10, position);
        int afterDivided = value/divisor;
        int afterMod = afterDivided % radix;
        return afterMod;
        // return value / (int)Math.pow(10, position) % radix
    }

}

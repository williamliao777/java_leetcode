package Algorithm.Sorting;

public class QuickSort {

    public static void main(String[] arg) {

        int[] unSortArray = {20,35,-15,7,55,1,-22};

        QuickSort(unSortArray, 0, unSortArray.length);

        for (int value : unSortArray) {
            System.out.println(value);
        }

    }

    public static void QuickSort(int[] unSortedArray, int start, int end){

        if(end - start < 2){
            return;
        }

        int pivotIndex = partition(unSortedArray, start, end);
        QuickSort(unSortedArray, 0, pivotIndex);
        QuickSort(unSortedArray, pivotIndex+1, end);
    }

    public static int partition(int[] unSortedArray, int start, int end){

        int i = start;
        int j = end;
        int pivot = unSortedArray[start];

        while(j > i){

            while(j > i && pivot <= unSortedArray[--j]);

            if(i < j){
                unSortedArray[i] = unSortedArray[j];
            }

            while(j > i && pivot >= unSortedArray[++i]);

            if(i < j){
                unSortedArray[j] = unSortedArray[i];
            }

        }

        unSortedArray[i] = pivot;
        return i;

    }


}

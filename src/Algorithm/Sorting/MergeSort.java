package Algorithm.Sorting;

import java.lang.reflect.Array;

public class MergeSort {

    public static void main(String[] arg) {

        int[] unSortArray = {20,35,-15,7,55,1,-22};
//        int[] unSortArray = {36,32,34,33}; //ex to understand

        splitSort(unSortArray, 0, unSortArray.length);

        for (int value : unSortArray) {
            System.out.println(value);
        }

    }

    public static void splitSort(int[] unSortedArray, int start, int end){

        if(end - start < 2){
            return;
        }

        int mid = (start + end) / 2;
        splitSort(unSortedArray, start, mid);
        splitSort(unSortedArray, mid, end);
        merge(unSortedArray, start, mid, end);

    }

    public static void merge(int[] unSortedArray, int start, int mid, int end){

        //左右半邊的陣列都是已經merged 所以假如左邊的最後一個(mid-1) < 右半邊的第一個(mid) 則排序完成
        if(unSortedArray[mid -1] <= unSortedArray[mid]){
            return;
        }

        int i = start;
        int j = mid;
        int tempIndex = 0;

        int[] temp = new int[end - start];

        //ex [2,5,8] [1,7]
        //   [1,2,5,7,8]
        while(i < mid && j < end){
            temp[tempIndex++] = unSortedArray[i] <= unSortedArray[j] ? unSortedArray[i++] : unSortedArray[j++];
        }

        //左半邊剩的最後一個  System.arraycopy(來源, 起始索引, 目的, 起始索引, 複製長度)
        //ex [32,36], [33,34]
        //   [32,33,34, x]   36   start + tempIndex(3)
        // i 會是最後一個沒被排到的 所以起始索引為i  mid-i 是算出左邊還有幾個要被複製通常剩一個 為1
        System.arraycopy(unSortedArray, i, unSortedArray, start + tempIndex, mid - i);//mid-i 檢查左邊有多少沒複製
        System.arraycopy(temp, 0, unSortedArray, start, tempIndex);
    }


}
